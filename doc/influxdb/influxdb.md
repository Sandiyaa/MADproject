# **InfluxDB** 
![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Influxdb_logo.svg/1200px-Influxdb_logo.svg.png)
- InfluxDB is an open-source time series database developed by InfluxData.
- Time series is a sequence of data points consisting of successive measurements made over time interval.
![](https://www.aptech.com/wp-content/uploads/2019/09/ts-pp-seasonality.jpg)
- It is written in Go and optimized for fast, high-availability storage and retrieval of time series data in fields such as operations monitoring, application metrics, Internet of Things sensor data, and real-time analytics.
- It also support processing data from Graphite.

![](https://upload.wikimedia.org/wikipedia/commons/e/e7/Graphite_%28log_monitoring_software%29.png)
## InfluxDB OSS  
* Download InfluxDB OSS
  -  Release version InfluxDB 1.8
* Install InfluxDB OSS  
  -  Installation Requirements:
     1.InfluxDB OSS networking ports
     2.Network Time Protocol (NTP)
  -  Process of correct configuration
`influxd -config /etc/influxdb/influxdb.conf`
  
  -  Setting environment variable
 `echo $INFLUXDB_CONFIG_PATH
/etc/influxdb/influxdb.conf
influxd`
* Get started with InfluxDB OSS
  -  Creating a database
`$ influx -precision rfc3339
Connected to http://localhost:8086 version 1.8.x
InfluxDB shell 1.8.x`
  
  -  Writing and exploring data
`<measurement>[,<tag-key>=<tag-value>...] <field-key>=<field-value>[,<field2-key>=<field2-value>...] [unix-nano-timestamp]`

![influx](https://www.influxdata.com/wp-content/uploads/Pre-canned-dashboards-influxdb.png)
### Key Concepts of InfluxDB
|            |            |          |
|------------|------------|----------|
|Database    |Field key   |Field set |
|Field value |Measurement |Point     | 
|Retention policy|Keys|Tagkey|
|Tag set|Tag value|Timestamp|

### Influx DB Glossary  
* **Aggregation**
  -  An InfluxQL function that returns an aggregated value across a set of points.
* **Batch**
  - A collection of data points in InfluxDB line protocol format, separated by newlines. 
* **Continuous query (CQ)**
  - An InfluxQL query that runs automatically and periodically within a database.
* **Database**
  - A logical container for users, retention policies, continuous queries, and time series data.
* **Duration** 
  - The attribute of the retention policy that determines how long InfluxDB stores data.
  - Data older than the duration are automatically dropped from the database. 
* **InfluxDB line protocol**
  - The text based format for writing points to InfluxDB.
* **Node** 
  - An independent influxd process.
* **Point**
  - In InfluxDB, a point represents a single data record, similar to a row in a SQL database table. 
  - Each point has a measurement, a tag set, a field key, a field value, and a timestamp;is uniquely identified by its series and timestamp.
* **Schema** 
  -  How the data are organized in InfluxDB. 
  -  The fundamentals of the InfluxDB schema are databases, retention policies, series, measurements, tag keys, tag values, and field keys. 
* **Series key**
  - A series key identifies a particular series by measurement, tag set, and field key.
* **Server**
  - A machine, virtual or physical, that is running InfluxDB. 
  - There should only be one InfluxDB process per server.
* **Shard**
  - A shard contains the actual encoded and compressed data, and is represented by a TSM file on disk. 
  - Every shard belongs to one and only one shard group. Multiple shards may exist in a single shard group. 
  - Each shard contains a specific set of series. All points falling on a given series in a given shard group will be stored in the same shard (TSM file) on disk.
* **Timestamp**
  - The date and time associated with a point. All time in InfluxDB is UTC.
* **TSM (Time Structured Merge tree)** 
  - The purpose-built data storage format for InfluxDB. 
  - TSM allows for greater compaction and higher write and read throughput than existing B+ or LSM tree implementations.
* **User**
  - There are two kinds of users in InfluxDB: Admin users have READ and WRITE access to all databases and full access to administrative queries and user management commands, Non-admin users have READ, WRITE, or ALL (both READ and WRITE) access per database.

### Working of InfluxDB
![](https://hackernoon.com/hn-images/0*Aw1A7GHp8-KMHpWY.)

### InfluxDB Features
- Specific query language like(Influx Query Language)
- Written entirely in Go. It compiles into a single binary with no external dependencies.
- Simple, high performing write and query HTTP APIs.
- No SQL and schemaless database.
- Retention policies for single measurement.
- Replication policies for each database.
- Tags allow series to be indexed for fast and efficient queries.
- Continuous queries automatically compute aggregate data to make frequent queries more efficient.

![](https://image.slidesharecdn.com/2-influxdb-160721111107/95/influxdb-and-time-series-data-13-638.jpg?cb=1469099555)

### Tools

*  **Influx command line interface (CLI)**
   - The InfluxDB command line interface (influx) includes commands to manage many aspects of InfluxDB, including databases, organizations, users, and tasks.

* **Influxd command**
   - The influxd command starts and runs all the processes necessary for InfluxDB to function.

* **Influx Inspect disk shard utility**
   - Influx Inspect is a tool designed to view detailed information about on disk shards, as well as export data from a shard to line protocol that can be inserted back into the database.

* **InfluxDB inch tool**
   - Use the InfluxDB inch tool to test InfluxDB performance. 
   - Adjust metrics such as the batch size, tag values, and concurrent write streams to test how ingesting different tag cardinalities and metrics affects performance.

* **Graphs and dashboards**
  - Use Chronograf or Grafana dashboards to visualize your time series data. 
![](https://2.bp.blogspot.com/-WzVFW6aLOp8/WrH2SoStraI/AAAAAAAABJM/pNKlVT22Jtcgx4Cm4y6z4EKl8OretsGUACLcBGAs/s1600/tickstack.PNG)

### Example protocol of InfluxDB
![lineprotocol](https://kapibara-sos.net/wp-content/uploads/2018/09/inflx01-1024x494.png)

## Plugins of InfluxDB
 - Telegraf plugins
   -  Telegraf is a plugin-driven agent that collects, processes, aggregates, and writes metrics. 
   - It supports four categories of plugins including **input, output, aggregator, and processor**.

![](https://lh3.googleusercontent.com/proxy/UInr5eaxvF4YjEv00T26LiqmOZrAiB4x0V52TSCZmRev1S_sqnqg6rPOMHFx8hwjX39rwJAGC45KnxCCFGiJhc0a3_p1dJS_iRoRXVHAndce)

### Classification of Telegraf plugin

|Plugin type|Plugin category|Operating system|Status|
|-----------|---------------|----------------|------|
|Input      |Applications   |macOS           |New|
|Output     |Build & Deploy |Linux           |Depricated|
|Aggregators|Cloud          |Windows         |
|Processor  |Containers     |
|           |Data Stores    |
|           |IoT            |
|           |Logging        |
|           |Messaging      |
|           |Networking     |
|           |Server         |
|           |System         |
|           |Web            |

![](https://www.coretechnologies.com/products/AlwaysUp/Apps/influxd-started.png)

### Tools:  
![](https://image.slidesharecdn.com/session7-influxdatainternalsbyryanbetts-190317204340/95/influxdata-internals-by-ryan-betts-11-638.jpg?cb=1553566453)
* **Influx CLI**  
  -  Use the influx CLI to interact with and manage both InfluxDB Cloud and InfluxDB OSS.
  -   Write and query data, generate InfluxDB templates, export data, manage organizations and users, and more.
![](https://user-images.githubusercontent.com/17724050/37395441-3bd7ab3e-279c-11e8-9287-02ab51401560.png)

* **Influxd CLI**  
  - Use the influxd CLI to start the InfluxDB OSS server and manage the InfluxDB storage engine.
![](https://static.wixstatic.com/media/595770_ee6be11d462f45fdb1fe636d711b6b86~mv2.png/v1/fill/w_1000,h_328,al_c,usm_0.66_1.00_0.01/595770_ee6be11d462f45fdb1fe636d711b6b86~mv2.png)
 
  - Restore data, rebuild the time series index (TSI), assess the health of the underlying storage engine, and more.
 ![](https://docs.influxdata.com/img/influxdb/2-0-data-explorer.png)
